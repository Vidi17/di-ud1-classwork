package es.vidal.ud1diclasswork.activitat1;

import javafx.application.Application;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class Activitat1 extends Application implements EventHandler<Event> {

    public final static int WIDTH = 50;

    private Label indicatorLabel;

    @Override
    public void start(Stage stage) {

        stage.setTitle("Actividad 1");
        Group board = new Group();

        for (int x = 0; x < 10 ; x++) {
            for (int y = 0; y < 10 ; y++) {
                Rectangle rectangle = new Rectangle(WIDTH,WIDTH);
                rectangle.setX(x * WIDTH);
                rectangle.setY(y * WIDTH);
                rectangle.setId("Coordenadas columna: " + x + " + fila: " + y);

                if ((x + y) % 2 == 0){
                    rectangle.setFill(Color.BLACK);
                } else {
                    rectangle.setFill(Color.WHITE);
                }
                rectangle.setOnMouseClicked(this);
                board.getChildren().add(rectangle);
            }
        }
        indicatorLabel = new Label("");
        indicatorLabel.setPadding(new Insets(20,20,20,20));
        VBox layout = new VBox();
        layout.setAlignment(Pos.TOP_CENTER);
        layout.getChildren().addAll(board,indicatorLabel);

        Scene theScene = new Scene(layout,800,550);
        stage.setScene(theScene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void handle(Event event) {

        Rectangle rectangle = (Rectangle) event.getSource();
        indicatorLabel.setText(rectangle.getId());
    }
}