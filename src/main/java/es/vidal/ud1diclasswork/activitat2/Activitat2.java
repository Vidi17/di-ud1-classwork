package es.vidal.ud1diclasswork.activitat2;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class Activitat2 extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        GridPane gridPane = new GridPane();
        stage.setTitle("Activitat 1");

        gridPane.setHgap(5);
        gridPane.setVgap(5);
        int center = 4 ;

        for (int row = 0; row <= 4; row++ ) {

            int  range = (1 +(2* row));
            int  startColumn = center - range/2 ;

            for(int i = 0 ; i < range; i++){
                Circle circle  = new Circle(20 , Color.BLUE);
                circle.setStroke(Color.BLACK);
                circle.setStrokeWidth(3);
                gridPane.add(circle,startColumn+i , row);
            }
        }

    Scene scene = new Scene(gridPane, 400, 300);
        stage.setScene(scene);
        stage.show();
    }
}
