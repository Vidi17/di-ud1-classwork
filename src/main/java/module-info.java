module es.vidal.ud1diclasswork {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires validatorfx;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;
    requires eu.hansolo.tilesfx;

    opens es.vidal.ud1diclasswork to javafx.fxml;
    exports es.vidal.ud1diclasswork;
    exports es.vidal.ud1diclasswork.activitat1;
    opens es.vidal.ud1diclasswork.activitat1 to javafx.fxml;
    exports es.vidal.ud1diclasswork.activitat2;
    opens es.vidal.ud1diclasswork.activitat2 to javafx.fxml;
    exports es.vidal.ud1diclasswork.appExemple;
    opens es.vidal.ud1diclasswork.appExemple to javafx.fxml;
}